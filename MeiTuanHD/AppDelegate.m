//
//  AppDelegate.m
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/8/27.
//

#import "AppDelegate.h"
#import "HMHomeViewController.h"
#import "HMNavigationController.h"
#import "HMDetailViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
   
    if (!@available(iOS 13.0, *)) {
        //13.0 之前
        //1.创建 window
        self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        
        //2.设置根控制器
        self.window.rootViewController = [[HMNavigationController alloc] initWithRootViewController:[HMHomeViewController new]];
        
        //3.设置 window 可见
        [self.window makeKeyAndVisible];
    }
    return YES;
}

#pragma mark - UISceneSession lifecycle
- (UISceneConfiguration *)application:(UIApplication *)application configurationForConnectingSceneSession:(UISceneSession *)connectingSceneSession options:(UISceneConnectionOptions *)options {
    // Called when a new scene session is being created.
    // Use this method to select a configuration to create the new scene with.
    return [[UISceneConfiguration alloc] initWithName:@"Default Configuration" sessionRole:connectingSceneSession.role];
}


- (void)application:(UIApplication *)application didDiscardSceneSessions:(NSSet<UISceneSession *> *)sceneSessions {
    // Called when the user discards a scene session.
    // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
    // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
}


@end
