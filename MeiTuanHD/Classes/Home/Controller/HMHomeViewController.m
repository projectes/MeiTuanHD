//
//  HMHomeViewController.m
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/8/27.
//

#import "HMHomeViewController.h"
#import "HMHomeNavView.h"
#import "HMCategoryViewController.h"
#import "HMDistricViewController.h"
#import "HMCityModel.h"
#import "HMDistrictsModel.h"
#import "HMCategoryModel.h"
#import "HMSortViewController.h"
#import "HMSortModel.h"
#import "HMDealCell.h"
#import "HMDealModel.h"
#import "HMDetailViewController.h"
#import "HMSearchViewController.h"
#import "HMNavigationController.h"
#import "HMMapViewController.h"
#import "HMCollectionViewController.h"

@interface HMHomeViewController ()<AwesomeMenuDelegate>

/* 通知发送选中的城市名 */
@property (nonatomic, copy) NSString *selectCityName;
/* 通知发送选中的分类名 */
@property (nonatomic, copy) NSString *selectCategoryName;
/* 通知发送选中的区域名 */
@property (nonatomic, copy) NSString *selectDistrictName;
/* 通知发送选中的排序 */
@property (nonatomic, strong) NSNumber *selectSortNumber;

/* 区域导航栏 View */
@property (nonatomic, strong) HMHomeNavView *districtView;

/* 分类导航栏 View */
@property (nonatomic, strong) HMHomeNavView *categoryView;

/* 排序导航栏 View */
@property (nonatomic, strong) HMHomeNavView *sortView;

@end

@implementation HMHomeViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    
    //设置左边导航栏
    [self setupLeftNav];
    
    // 设置右边导航栏
    [self setupRighNav];
    
    //添加菜单按钮
    [self addAwesomeMenu];
    
    //一开始就刷新 --> 进入刷新状态 --> 就会自动调用上方 block 绑定的方法
    [self.collectionView.mj_header beginRefreshing];
}



#pragma mark 菜单按钮
-(void)addAwesomeMenu{
    
    //1. 中间的startItem
    AwesomeMenuItem *startItem = [[AwesomeMenuItem alloc]
                                  initWithImage:[UIImage imageNamed:@"icon_pathMenu_background_normal"]
                                  highlightedImage:[UIImage imageNamed:@"icon_pathMenu_background_highlighted"]
                                  ContentImage:[UIImage imageNamed:@"icon_pathMenu_mainMine_normal"]
                                  highlightedContentImage:nil];
    //2. 添加其他几个按钮
    AwesomeMenuItem *item0 = [[AwesomeMenuItem alloc]
                              initWithImage:[UIImage imageNamed:@"bg_pathMenu_black_normal"]
                              highlightedImage:nil
                              ContentImage:[UIImage imageNamed:@"icon_pathMenu_mainMine_normal"]
                              highlightedContentImage:[UIImage imageNamed:@"icon_pathMenu_mainMine_highlighted"]];
    
    AwesomeMenuItem *item1 = [[AwesomeMenuItem alloc]
                              initWithImage:[UIImage imageNamed:@"bg_pathMenu_black_normal"]
                              highlightedImage:nil
                              ContentImage:[UIImage imageNamed:@"icon_pathMenu_collect_normal"]
                              highlightedContentImage:[UIImage imageNamed:@"icon_pathMenu_collect_highlighted"]];
    
    AwesomeMenuItem *item2 = [[AwesomeMenuItem alloc]
                              initWithImage:[UIImage imageNamed:@"bg_pathMenu_black_normal"]
                              highlightedImage:nil
                              ContentImage:[UIImage imageNamed:@"icon_pathMenu_scan_normal"]
                              highlightedContentImage:[UIImage imageNamed:@"icon_pathMenu_scan_highlighted"]];
    
    AwesomeMenuItem *item3 = [[AwesomeMenuItem alloc]
                              initWithImage:[UIImage imageNamed:@"bg_pathMenu_black_normal"]
                              highlightedImage:nil
                              ContentImage:[UIImage imageNamed:@"icon_pathMenu_more_normal"]
                              highlightedContentImage:[UIImage imageNamed:@"icon_pathMenu_more_highlighted"]];
    
    NSArray *items = @[item0, item1, item2, item3];
    
    //3. 创建菜单按钮
    AwesomeMenu *menu = [[AwesomeMenu alloc] initWithFrame:CGRectZero startItem:startItem menuItems:items];
    [self.view addSubview: menu];
    
    //4. 禁止中间按钮旋转
    menu.rotateAddButton = NO;
    
    //5. 旋转角度
    menu.menuWholeAngle = M_PI_2;
    
    //6. 设置按钮位置
    menu.startPoint = CGPointMake(60, -80);
    [menu mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.equalTo(self.view);
    }];
    //[menu autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.view];
    //[menu autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self.view];
    
    //7. 设置代理
    menu.delegate = self;
    
    //8. 更改透明度
    menu.alpha = 0.6;
}

#pragma mark AwesomeMenu 代理方法
- (void)awesomeMenu:(AwesomeMenu *)menu didSelectIndex:(NSInteger)idx{
    
    //1. 透明度
    menu.alpha = 0.6;
    //2. 更改图像
    menu.contentImage = [UIImage imageNamed:@"icon_pathMenu_mainMine_normal"];
    switch (idx) {
        case 0:
            break;
        case 1:
        {
            HMNavigationController *nav = [[HMNavigationController alloc] initWithRootViewController:[HMCollectionViewController new]];
            nav.modalPresentationStyle = UIModalPresentationFullScreen;
           [self presentViewController:nav animated:YES completion:nil];
        }
            break;
        case 2:
            break;
        case 3:
            break;
    }
   
 
  
}


- (void)awesomeMenuWillAnimateOpen:(AwesomeMenu *)menu{
    [UIView animateWithDuration:0.25 animations:^{
            //1. 透明度
            menu.alpha = 1;
        
            //2. 更改图像
            menu.contentImage = [UIImage imageNamed:@"icon_pathMenu_cross_normal"];
    }];
        
}

- (void)awesomeMenuWillAnimateClose:(AwesomeMenu *)menu{
    [UIView animateWithDuration:0.25 animations:^{
            //1. 透明度
            menu.alpha = 0.6;
            //2. 更改图像
            menu.contentImage = [UIImage imageNamed:@"icon_pathMenu_mainMine_normal"];
    }];
}

//- (void)awesomeMenuDidFinishAnimationOpen:(AwesomeMenu *)menu{
//    //1. 透明度
//    menu.alpha = 1;
//
//    //2. 更改图像
//    menu.contentImage = [UIImage imageNamed:@"icon_pathMenu_cross_normal"];
//
//}
//
//- (void)awesomeMenuDidFinishAnimationClose:(AwesomeMenu *)menu{
//    //1. 透明度
//    menu.alpha = 0.6;
//
//    //2. 更改图像
//    menu.contentImage = [UIImage imageNamed:@"icon_pathMenu_mainMine_normal"];
//}

#pragma mark  - 城市通知
-(void)cityDidChangeNotification:(NSNotification*)notification{
    //1.1 获取通知的值
    self.selectCityName =  notification.userInfo[HMCityNameKey];
    
    //清空其他三项数据
    self.selectCategoryName = nil;
    self.selectDistrictName = nil;
    self.selectSortNumber = nil;
    
    //1.2 获取导航栏 View 直接更新标题
    [self.districtView setTitle:[NSString stringWithFormat:@"%@-全部",self.selectCityName]];
    
    //1.3 清空子标题
    [self.districtView setSubtitle:@""];
    
    //获取导航栏 View
    //UIBarButtonItem *distrcitItem = self.navigationItem.leftBarButtonItems[2];
    //HMHomeNavView *homeNavView = distrcitItem.customView;
    
    //发送请求通知 --> 会调用刷新绑定的方法 --> 会有动画下移
    [self.collectionView.mj_header beginRefreshing];
   // [self loadNewDeal];
}

#pragma mark  - 区域通知
-(void)districtDidChangeNotification:(NSNotification*)notification{
    //1.左边模型
    HMDistrictsModel *selectDistrictModel =notification.userInfo[HMDistrictModelKey];
    //2.右边子标题
    NSString *selectDistrictSubtitle = notification.userInfo[HMDistrictSubtitleKey];
    
    
    /**
     1. 发送右边 右边有值，且不等于 “全部”
     2. 发送左边 没有子分类数据，就发送左边，且不等于"全部”
     3. 不发送 没有需求进行筛选 或者 点击了 @“全部”
     */
    //获取选中的区域 --> 为了发送请求
    
    if(selectDistrictSubtitle == nil || [selectDistrictSubtitle isEqualToString:@"全部"]){
        self.selectDistrictName = selectDistrictModel.name;
    }else{
        //2.发送右边
        self.selectDistrictName = selectDistrictSubtitle;
    }
    
    //3.不发送
    if([self.selectDistrictName isEqualToString:@"全部"]){
        self.selectDistrictName = nil;
    }
    
    
    //3.1 获取导航栏 View 直接更新标题
    [self.districtView setTitle:[NSString stringWithFormat:@"%@-%@",self.selectCityName,selectDistrictModel.name]];
    
    //3.2 清空子标题
    [self.districtView setSubtitle:selectDistrictSubtitle];
    
    //3.取消 popover 的显示
    [self dismissViewControllerAnimated:YES completion:nil];
    
    //发送请求通知
    [self.collectionView.mj_header beginRefreshing];
}

#pragma mark  - 分类通知
-(void)categoryDidChangeNotifacation:(NSNotification*)notification{
    
    //1.左边模型
    HMCategoryModel *selectCategoryModel =notification.userInfo[HMCategoryModelKey];
    //2.右边子标题
    NSString *selectCategorySubtitle = notification.userInfo[HMCategorySubtitleKey];
    
    /**
     1. 发送右边 右边有值，且不等于 “全部”
     2. 发送左边 没有子分类数据，就发送左边，且不等于"全部”
     3. 不发送 没有需求进行筛选 或者 点击了 @“全部分类”
     */
    //获取选中的分类 --> 为了发送请求
    
    //1.发送左边
    if(selectCategorySubtitle == nil || [selectCategorySubtitle isEqualToString:@"全部"]){
        self.selectCategoryName = selectCategoryModel.name;
    }else{
        //2.发送右边
        self.selectCategoryName = selectCategorySubtitle;
    }
    
    //3.不发送
    if([self.selectCategoryName isEqualToString:@"全部分类"]){
        self.selectCategoryName = nil;
    }
    
    //3.1 获取导航栏 View 直接更新标题
    [self.categoryView setTitle:selectCategoryModel.name];
    //3.2 设置子标题
    [self.categoryView setSubtitle:selectCategorySubtitle];
    //3.2 设置图标
    [self.categoryView setIcon:selectCategoryModel.icon  hightIcon:selectCategoryModel.highlighted_icon];
    //3.取消 popover 的显示
    [self dismissViewControllerAnimated:YES completion:nil];
    
    //发送请求通知
    [self.collectionView.mj_header beginRefreshing];
}

#pragma mark  - 排序通知
-(void)sortDidChangeNotifacation:(NSNotification*)notification{
    //1.获取模型
    HMSortModel *selectSortModel = notification.userInfo[HMSortModelKey];
    
    //记录排序的 number 值
    self.selectSortNumber = selectSortModel.value;
    
    //1.3 设置子标题
    [self.sortView setSubtitle:selectSortModel.label];
    
    //2.取消 popover 的显示
    [self dismissViewControllerAnimated:YES completion:nil];
    
    //发送请求通知
    [self.collectionView.mj_header beginRefreshing];
}

#pragma mark 添加观察者方法
- (void)viewWillAppear:(BOOL)animated{
    //视图将要出现的时候,注册通知
    [super viewWillDisappear:animated];
    //注册城市选择选择通知
    [HMNotificationCenter addObserver:self selector:@selector(cityDidChangeNotification:) name:HMCityDidChangeNotifacation object:nil];
    
    //注册区域选择通知
    [HMNotificationCenter addObserver:self selector:@selector(districtDidChangeNotification:) name:HMDistrictDidChangeNotifacation object:nil];
    
    //注册选择分类通知
    [HMNotificationCenter addObserver:self selector:@selector(categoryDidChangeNotifacation:) name:HMCategoryDidChangeNotifacation object:nil];
    
    //注册排序选中通知
    [HMNotificationCenter addObserver:self selector:@selector(sortDidChangeNotifacation:) name:HMSortDidChangeNotifacation object:nil];
}

#pragma mark 移除观察者
-(void)viewWillDisappear:(BOOL)animated{

    //当视图消失的时候移除通知
    [super viewWillDisappear:animated];
    [HMNotificationCenter removeObserver:self];
}

//- (void)dealloc{
//    [HMNotificationCenter removeObserver:self];
//}

#pragma mark 设置左边导航栏
-(void)setupLeftNav {
    
    //1.Logo
    UIBarButtonItem *logoItem  = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"icon_meituan_logo"]
                                  imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:nil action:nil];
    logoItem.enabled = NO;
    
    //2.分类
    HMHomeNavView *categoryView = [HMHomeNavView homeNavView];
    UIBarButtonItem *categoryItem = [[UIBarButtonItem alloc] initWithCustomView:categoryView];
    categoryItem.width = 160;
    [categoryView setTitle:@"全部分类"];
    [categoryView setSubtitle:@""];
    [categoryView setIcon:@"icon_category_-1" hightIcon:@"icon_category_highlighted_-1"];
    //属性赋值
    self.categoryView =categoryView;
    //添加按钮点击事件
    [categoryView addTarget:self action:@selector(categoryClick)];
    
    //3.区域
    HMHomeNavView *districtView = [HMHomeNavView homeNavView];
    UIBarButtonItem *distrctItem = [[UIBarButtonItem alloc] initWithCustomView:districtView];
    distrctItem.width = 160;
    [districtView addTarget:self action:@selector(districtClick)];
    [districtView setTitle:@"北京-全部"];
    [districtView setSubtitle:@""];
    //属性赋值
    self.districtView = districtView;
    
    //4.排序
    HMHomeNavView *sortView = [HMHomeNavView homeNavView];
    UIBarButtonItem *sortItem = [[UIBarButtonItem alloc] initWithCustomView:sortView];
    sortItem.width = 160;
    [sortView addTarget:self action:@selector(sortClick)];
    [sortView setTitle:@"排序"];
    [sortView setSubtitle:@"默认排序"];
    //属性赋值
    self.sortView = sortView;
    
    self.navigationItem.leftBarButtonItems = @[logoItem ,categoryItem ,distrctItem, sortItem];
    
}

#pragma mark 分类按钮点击
-(void)categoryClick{
    //1.创建内容控制器
    HMCategoryViewController *categoryVC = [HMCategoryViewController new];
    
    //2.设置 popover 相关属性
    categoryVC.modalPresentationStyle = UIModalPresentationPopover;
    categoryVC.popoverPresentationController.barButtonItem = self.navigationItem.leftBarButtonItems[1];
    
    //3.模态显示控制器
    [self presentViewController:categoryVC animated:YES completion:nil];
    
}

#pragma mark 区域按钮点击
-(void)districtClick{
    //1.创建内容控制器
    HMDistricViewController *districVC = [HMDistricViewController new];
    
    //需要 cities.plist中获取，根据选中的城市名进行查找，找到了，就返回子区域数据，进行设置
    //1.1 如果选中的城市名有值
    if(self.selectCityName){
        //1.2 加载plist 并转模型
        NSMutableArray *cityArray = [NSMutableArray array];
        NSArray *cityPlist = [NSArray  arrayWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"cities.plist" ofType:nil]];
        
        for (NSDictionary *dict in cityPlist) {
            [cityArray addObject:[HMCityModel yy_modelWithJSON:dict]];
        }
        //1.3 遍历模型数据
        for (HMCityModel *cityModel in cityArray) {
            //1.4 判断模型的 name 属性是否跟选中的城市名相同
            if([cityModel.name isEqualToString:self.selectCityName]){
                //内容控制器,需要设置 districtArray
                //1.5 如果相同，那么将模型的子区域数据，赋值给区域控制器的属性
                districVC.districtArray = cityModel.districts;
                break;
            }
        }
    }
   
    //2.设置 popover 相关属性
    districVC.modalPresentationStyle = UIModalPresentationPopover;
    districVC.popoverPresentationController.barButtonItem = self.navigationItem.leftBarButtonItems[2];
    
    //3.模态显示控制器
    [self presentViewController:districVC animated:YES completion:nil];
}

#pragma mark 排序按钮点击
-(void)sortClick{
    //1.创建内容控制器
    HMSortViewController *sortVC = [HMSortViewController new];
    
    //2.设置 popover 相关属性
    sortVC.modalPresentationStyle = UIModalPresentationPopover;
    sortVC.popoverPresentationController.barButtonItem = self.navigationItem.leftBarButtonItems[3];
    
    //3.模态显示控制器
    [self presentViewController:sortVC animated:YES completion:nil];
}

#pragma mark 设置右边导航栏
-(void)setupRighNav {
    
    //1. 搜素
    UIBarButtonItem *searchItem = [UIBarButtonItem barbuttonItemWithTarget:self
         action:@selector(searchClick)icon:@"icon_search" highlighticon:@"icon_search_highlighted"];
    searchItem.customView.width=80;
    
    //地图
    UIBarButtonItem *mapItem  = [UIBarButtonItem barbuttonItemWithTarget:self
         action:@selector(mapClick) icon:@"icon_map" highlighticon:@"icon_map_highlighted"];
    mapItem.customView.width = 80;
    
    self.navigationItem.rightBarButtonItems = @[mapItem, searchItem];
}

#pragma mark 搜索按钮点击方法
-(void) searchClick{
    HMSearchViewController  *searchVC = [HMSearchViewController new];
    if(self.selectCityName){
        searchVC.selectCityName = self.selectCityName;
    }else{
        searchVC.selectCityName = @"北京";
    }
    HMNavigationController  *nav = [[HMNavigationController alloc] initWithRootViewController:searchVC];
    nav.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:nav animated:YES completion:nil];
 
}

#pragma mark 地图按钮点击方法
-(void) mapClick{
    HMMapViewController *mapVC = [HMMapViewController new];
    HMNavigationController *nav = [[HMNavigationController alloc] initWithRootViewController:mapVC];
    nav.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:nav animated:YES completion:nil];
    
}

#pragma mark 设置参数
-(void)setParams:(NSMutableDictionary *)params{
    
    //2.1 城市 -->  正常的程序，应该首次启动定位来获取城市 --> 咱们这里没有做
    params[@"city"] = self.selectCityName == nil ?  @"北京" : self.selectCityName;
    
    //2.2 分类
    if(self.selectCategoryName){
        params[@"category"] = self.selectCategoryName;
    }
   
    //2.3 区域
    if(self.selectDistrictName){
        params[@"region"] = self.selectDistrictName;
    }
    
    //2.4 排序
    if(self.selectSortNumber){
        params[@"sort"] = self.selectSortNumber;
    }
    
    //测试数据
    params[@"testCount"] = @(18);
}
@end
