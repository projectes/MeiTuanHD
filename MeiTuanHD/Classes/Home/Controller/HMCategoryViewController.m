//
//  HMCategoryViewController.m
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/8/30.
//

#import "HMCategoryViewController.h"
#import "HMDropdownView.h"
#import "HMCategoryModel.h"
#import "HMMateTool.h"

@interface HMCategoryViewController ()

@end

@implementation HMCategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //1.创建下拉菜单
    HMDropdownView *dropdownView = [HMDropdownView dropdownView];
    
    //2. 添加到视图中
    [self.view addSubview:dropdownView];
    
    //3.内容控制器的大小
    self.preferredContentSize  = dropdownView.size;
    
    //4.加载分类数据
    dropdownView.categorgArray = [HMMateTool categories];;
}



@end
