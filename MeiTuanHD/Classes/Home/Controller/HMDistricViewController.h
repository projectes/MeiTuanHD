//
//  HMDistricViewController.h
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/9/1.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HMDistricViewController : UIViewController

/** 提供属性，由homeVC 传值  */
@property (nonatomic, strong) NSArray *districtArray;

@end

NS_ASSUME_NONNULL_END
