//
//  HMHomeViewController.h
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/8/27.
//

#import <UIKit/UIKit.h>
#import "HMBaseCollectionViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HMHomeViewController : HMBaseCollectionViewController



@end

NS_ASSUME_NONNULL_END
