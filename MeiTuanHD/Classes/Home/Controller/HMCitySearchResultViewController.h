//
//  HMCitySearchResultViewController.h
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/9/2.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HMCitySearchResultViewController : UITableViewController

/** 传值搜索文字  -->  实时搜索  */
@property (nonatomic,copy)NSString *searchText;

@end

NS_ASSUME_NONNULL_END
