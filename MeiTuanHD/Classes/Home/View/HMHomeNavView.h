//
//  HMHomeNavView.h
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/8/30.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HMHomeNavView : UIView

/* .h 暴露属性更好，还是 .m 中封装属性更好

 1. 控件名称发生修改,.h 暴露就需要修改多个地方
 2. .h暴露，有可能在别的地方发生误修改， .m 中封装保护权限
 */

//@property (weak, nonatomic) IBOutlet UIButton *button;

/* 提供方法，用于绑定按钮的点击事件*/
-(void)addTarget:(id)target action:(SEL)action;

/* 设置标题 */
-(void)setTitle:(NSString *) title;

/* 设置子标题 */
-(void)setSubtitle:(NSString *) subtitle;

/* 设置图标及高亮图标 */
-(void)setIcon:(NSString *)icon hightIcon:(NSString *)hightIcon;

/* 提供一个类方法, 加载 xib*/
+ (instancetype) homeNavView;

@end

NS_ASSUME_NONNULL_END
