//
//  HMDropdownView.h
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/8/30.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HMDropdownView : UIView

/**提供属性，--> categoryVC中加载并传值*/
@property (nonatomic,strong) NSArray *categorgArray;

/**提供属性，--> districtVC中加载并传值*/
@property (nonatomic,strong) NSArray *districtArray;


/* 提供类方法，加载 xib 文件 */
+ (instancetype) dropdownView;

@end

NS_ASSUME_NONNULL_END
