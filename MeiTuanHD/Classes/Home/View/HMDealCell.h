//
//  HMDealCell.h
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/9/7.
//

#import <UIKit/UIKit.h>


NS_ASSUME_NONNULL_BEGIN

@class HMDealModel;

@interface HMDealCell : UICollectionViewCell

/* 通知发送选中的排序 */
@property (nonatomic, strong) HMDealModel *dealModel;


/* 提供类方法，加载 xib*/
+(instancetype) dealCell;


/** 增加 block 属性 */
@property (nonatomic, copy) void(^dealCellDidClickBlock)();

@end

NS_ASSUME_NONNULL_END
