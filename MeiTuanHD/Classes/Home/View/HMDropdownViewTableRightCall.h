//
//  HMDropdownViewTableRightCall.h
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/8/31.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HMDropdownViewTableRightCall : UITableViewCell

/* 创建右边的表格 */
+(instancetype)dropdownViewTableRightCellWithTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END
