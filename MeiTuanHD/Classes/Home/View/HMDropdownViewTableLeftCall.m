//
//  HMDropdownViewTableLeftCall.m
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/8/31.
//

#import "HMDropdownViewTableLeftCall.h"

@implementation HMDropdownViewTableLeftCall


/* 创建左边的表格 */
+(instancetype)dropdownViewTableLeftCellWithTableView:(UITableView *)tableView{
    static NSString *identifier = @"leftCell";
    HMDropdownViewTableLeftCall *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil){
        cell = [[HMDropdownViewTableLeftCall alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
        //设置背景
        cell.backgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_dropdown_leftpart"]];
        //设置选中背景色
        cell.selectedBackgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_dropdown_left_selected"]];
    }
    return cell;
}

@end
