//
//  HMHomeNavView.m
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/8/30.
//

#import "HMHomeNavView.h"

@interface HMHomeNavView()

@property (weak, nonatomic) IBOutlet UIButton *coverButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLable;

@end

@implementation HMHomeNavView

/* 提供一个类方法, 加载 xib*/
+ (instancetype) homeNavView{
    return [[[NSBundle mainBundle] loadNibNamed:@"HMHomeNavView" owner:nil options:nil] firstObject];
}

/* 提供方法，用于绑定按钮的点击事件*/
-(void)addTarget:(id)target action:(SEL)action{
    [self.coverButton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
}

- (void)awakeFromNib{
    self.autoresizingMask = UIViewAutoresizingNone;
}


/* 设置标题 */
-(void)setTitle:(NSString *) title{
    self.titleLabel.text = title;
}

/* 设置子标题 */
-(void)setSubtitle:(NSString *) subtitle{
    self.subTitleLable.text = subtitle;
}

/* 设置图标及高亮图标 */
-(void)setIcon:(NSString *)icon hightIcon:(NSString *)hightIcon{
    [self.coverButton setImage:[UIImage imageNamed:icon] forState:UIControlStateNormal];
    [self.coverButton setImage:[UIImage imageNamed:hightIcon] forState:UIControlStateHighlighted];
}

@end
