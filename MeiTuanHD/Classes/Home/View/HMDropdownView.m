//
//  HMDropdownView.m
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/8/30.
//

#import "HMDropdownView.h"
#import "HMCategoryModel.h"
#import "HMDropdownViewTableLeftCall.h"
#import "HMDropdownViewTableRightCall.h"
#import "HMDistrictsModel.h"

@interface HMDropdownView()<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *leftTableView;
@property (weak, nonatomic) IBOutlet UITableView *rightTableView;

/* 记录分类左边选中的模型*/
@property (nonatomic, strong) HMCategoryModel *selectCategoryLeftModel;

/* 记录分类左边选中的模型*/
@property (nonatomic, strong) HMDistrictsModel *selectDistricLeftModel;

@end

@implementation HMDropdownView

#pragma mark - TableView 数据源方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    /**
     1. 对之前的代码，增加一层 if 判断，同时需要对 else 里的代码，
     2. 将category 换成 district
     */
    //1.1 判断分类有值
//    if(self.categorgArray){
//        //1.2 判断分类左边数据
//        if(tableView==self.leftTableView){
//
//        }else{
//        //1.3 判断分类右边数据
//        }
//    }else{
//        //2.1判断区域数据有值
//        //1.2 判断区域左边数据
//        if(tableView==self.leftTableView){
//
//        }else{
//        //1.3 判断区域右边数据
//        }
//    }
    
    //如果分类数据有值
    if(self.categorgArray){
        //判断是否是左边的表格
        if(tableView ==self.leftTableView){
            return self.categorgArray.count;
        }else{
            //tableView 点击 Cell 的方法记录选中的模型 --> 需要生产一个属性
            return self.selectCategoryLeftModel.subcategories.count;
        }
    }else{
        //区域数据有值
        if(tableView ==self.leftTableView){
            return self.districtArray.count;
        }else{
            //tableView 点击 Cell 的方法记录选中的模型 --> 需要生产一个属性
            return self.selectDistricLeftModel.subdistricts.count;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
 
    //分类数据有值
    if(self.categorgArray){
        
        if(tableView == self.leftTableView){
            
            //0.创建左边表格
            HMDropdownViewTableLeftCall* cell = [HMDropdownViewTableLeftCall dropdownViewTableLeftCellWithTableView:tableView];
            //1.获取模型数据
            HMCategoryModel *categoryModel = self.categorgArray[indexPath.row];
            
            //2.赋值
            cell.textLabel.text = categoryModel.name;
            
            //3.显示箭头
            if(categoryModel.subcategories.count){
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }else{
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
            
            //4.显示图标
            cell.imageView.image = [UIImage imageNamed:categoryModel.icon];
            
            //5.设置高亮图标
            cell.imageView.highlightedImage = [UIImage imageNamed:categoryModel.highlighted_icon];
            
            return cell;
            
        }else{
            HMDropdownViewTableRightCall *cell = [HMDropdownViewTableRightCall dropdownViewTableRightCellWithTableView:tableView];
            //1.赋值
            cell.textLabel.text = self.selectCategoryLeftModel.subcategories[indexPath.row];
            return cell;
        }
        
    }else{
        //区域代码有值
        if(tableView == self.leftTableView){
            
            //0.创建左边表格
            HMDropdownViewTableLeftCall* cell = [HMDropdownViewTableLeftCall dropdownViewTableLeftCellWithTableView:tableView];
            //1.获取模型数据
            HMDistrictsModel *districtsModel = self.districtArray[indexPath.row];
            
            //2.赋值
            cell.textLabel.text = districtsModel.name;
            
            //3.显示箭头
            if(districtsModel.subdistricts.count){
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }else{
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
            return cell;
            
        }else{
            HMDropdownViewTableRightCall *cell = [HMDropdownViewTableRightCall dropdownViewTableRightCellWithTableView:tableView];
            //1.赋值
            cell.textLabel.text = self.selectDistricLeftModel.subdistricts[indexPath.row];
            return cell;
        }
    }
}


#pragma mark - TableView 选中的方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //分类数据有值
    if(self.categorgArray){
        // 左边选中才需要记录
        if(tableView == self.leftTableView){
            //对选中的模型赋值
            self.selectCategoryLeftModel = self.categorgArray[indexPath.row];
            
            if(self.selectCategoryLeftModel.subcategories.count == 0){
                //发送通知
                [HMNotificationCenter postNotificationName:HMCategoryDidChangeNotifacation object:nil userInfo:@{HMCategoryModelKey:self.selectCategoryLeftModel}];
            }
        }else{
            //发送通知
            [HMNotificationCenter postNotificationName:HMCategoryDidChangeNotifacation object:nil userInfo:@{HMCategoryModelKey:self.selectCategoryLeftModel,
                                                       HMCategorySubtitleKey:self.selectCategoryLeftModel.subcategories[indexPath.row]}];
        }
    }else{
        //区域代码有值
        if(tableView == self.leftTableView){
            //1.记录左边选中的模型
            self.selectDistricLeftModel = self.districtArray[indexPath.row];
           
            //2.如果没有子区域数据，直接发送通知
            if(self.selectDistricLeftModel.subdistricts.count == 0){
            //发送通知
            [HMNotificationCenter postNotificationName:HMDistrictDidChangeNotifacation object:nil userInfo:@{HMDistrictModelKey:self.selectDistricLeftModel}];
            }
        }else{
            //发送通知
            [HMNotificationCenter postNotificationName:HMDistrictDidChangeNotifacation object:nil userInfo:@{HMDistrictModelKey:self.selectDistricLeftModel,
                                                       HMDistrictSubtitleKey:self.selectDistricLeftModel.subdistricts[indexPath.row]}];
        }
    }
 
    //刷新右边的数据
    [self.rightTableView reloadData];
}


+ (instancetype) dropdownView{
    
    return [[[NSBundle mainBundle] loadNibNamed:@"HMDropdownView" owner:nil options:nil]firstObject];
}

- (void)awakeFromNib{
    /**
         1. 当一个较大的视图，放到较小的视图里，视图就会发生压缩行为
         2. autolayout 会勾选autoresizingMask 相关的属性，导致压缩/拉伸时，视图也会跟着变化，如果压缩的太狠，就会导致内容压缩看不见
     */
    self.autoresizingMask = UIViewAutoresizingNone;
}

@end
