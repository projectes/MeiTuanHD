//
//  HMCenterLineLabel.h
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/9/9.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HMCenterLineLabel : UILabel

@end

NS_ASSUME_NONNULL_END
