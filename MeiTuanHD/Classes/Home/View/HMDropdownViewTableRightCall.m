//
//  HMDropdownViewTableRightCall.m
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/8/31.
//

#import "HMDropdownViewTableRightCall.h"

@implementation HMDropdownViewTableRightCall


/* 创建左边的表格 */
+(instancetype)dropdownViewTableRightCellWithTableView:(UITableView *)tableView{
    static NSString *identifier = @"rightCell";
    HMDropdownViewTableRightCall *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil){
        cell = [[HMDropdownViewTableRightCall alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
        //设置背景
        cell.backgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_dropdown_rightpart"]];
        //设置选中背景色
        cell.selectedBackgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_dropdown_right_selected"]];
    }
    return cell;
}

@end
