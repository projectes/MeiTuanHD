//
//  HMDropdownViewTableLeftCall.h
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/8/31.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HMDropdownViewTableLeftCall : UITableViewCell

/* 创建左边的表格 */
+(instancetype)dropdownViewTableLeftCellWithTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END
