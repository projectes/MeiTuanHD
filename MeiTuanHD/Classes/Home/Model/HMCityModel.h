//
//  HMCityModel.h
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/9/3.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HMCityModel : NSObject
/** 城市名字 **/
@property (nonatomic, copy) NSString *name;

/** 城市名字拼音 **/
@property (nonatomic, copy) NSString *pinYin;

/** 城市名字拼音首字母 **/
@property (nonatomic, copy) NSString *pinYinHead;

/** 选择街道的数组 **/
@property (nonatomic, strong) NSArray *districts;



@end

NS_ASSUME_NONNULL_END
