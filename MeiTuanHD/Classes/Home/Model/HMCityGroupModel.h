//
//  LMYCityGroupModel.h
//  MeiTuanHD
//
//  Created by LEE on 16/9/16.
//  Copyright © 2016年 LEE. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HMCityGroupModel : NSObject
@property (nonatomic, strong) NSArray *cities;
@property (nonatomic, copy) NSString *title;
@end
