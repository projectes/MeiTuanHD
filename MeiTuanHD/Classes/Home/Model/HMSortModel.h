//
//  HMSortModel.h
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/9/7.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HMSortModel : NSObject
/** 名字 */
@property (nonatomic, copy) NSString *label;
/** 值 --> 给服务器发送的 */
@property (nonatomic, strong) NSNumber *value;
@end

NS_ASSUME_NONNULL_END
