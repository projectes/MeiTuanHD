//
//  HMDealModel.m
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/9/9.
//

#import "HMDealModel.h"

@implementation HMDealModel

// 返回一个 Dict ，将 Model 属性名对映射到 JSON 的 Key
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"desc"  : @"description"};
}

// 直接添加以下代码即可自动完成
- (void)encodeWithCoder:(NSCoder *)aCoder { [self yy_modelEncodeWithCoder:aCoder]; }
- (id)initWithCoder:(NSCoder *)aDecoder { self = [super init]; return [self yy_modelInitWithCoder:aDecoder]; }
- (id)copyWithZone:(NSZone *)zone { return [self yy_modelCopy]; }
- (NSUInteger)hash { return [self yy_modelHash]; }
- (BOOL)isEqual:(id)object { return [self yy_modelIsEqual:object]; }
- (NSString *)description { return [self yy_modelDescription]; }

@end
