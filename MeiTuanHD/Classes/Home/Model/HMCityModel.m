//
//  HMCityModel.m
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/9/3.
//

#import "HMCityModel.h"
#import "HMDistrictsModel.h"

@implementation HMCityModel

// 返回容器类中的所需要存放的数据类型 (以 Class 或 Class Name 的形式)。
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"districts" : [HMDistrictsModel class]};
}

@end
