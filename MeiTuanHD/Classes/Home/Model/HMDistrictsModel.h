//
//  HMDistrictsModel.h
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/9/3.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HMDistrictsModel : NSObject

/** 名字*/
@property (nonatomic, copy) NSString *name;

/** 子区域数据*/
@property (nonatomic, strong) NSArray *subdistricts;

@end

NS_ASSUME_NONNULL_END
