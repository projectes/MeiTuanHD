//
//  HMSearchViewController.h
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/9/17.
//

#import <UIKit/UIKit.h>
#import "HMBaseCollectionViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HMSearchViewController : HMBaseCollectionViewController

@property (nonatomic, copy) NSString *selectCityName;

@end

NS_ASSUME_NONNULL_END
