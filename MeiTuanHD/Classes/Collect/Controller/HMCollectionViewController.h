//
//  HMCollectionViewController.h
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/9/28.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HMCollectionViewController : UICollectionViewController

@end

NS_ASSUME_NONNULL_END
