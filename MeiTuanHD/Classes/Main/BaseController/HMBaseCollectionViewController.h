//
//  HMBaseCollectionViewController.h
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/9/17.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HMBaseCollectionViewController : UICollectionViewController

/**
 父类提供了一个方法 --> 子类也会有此方法
 */
-(void)setParams:(NSMutableDictionary *)params;

@end

NS_ASSUME_NONNULL_END
