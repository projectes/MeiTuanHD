//
//  HMNavigationController.h
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/8/30.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HMNavigationController : UINavigationController

@end

NS_ASSUME_NONNULL_END
