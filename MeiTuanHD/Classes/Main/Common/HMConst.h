//
//  HMConst.h
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/9/6.
//

#import <Foundation/Foundation.h>




//extern: 引用本类或者其他类中使用
//UIKIT_EXTERN: 自动区分C语言和C++
extern NSString *const HMCityDidChangeNotifacation;
extern NSString *const HMCityNameKey;

// 分类通知
extern NSString *const HMCategoryDidChangeNotifacation;
extern NSString *const HMCategoryModelKey;
extern NSString *const HMCategorySubtitleKey;

// 区域通知
extern NSString *const HMDistrictDidChangeNotifacation;
extern NSString *const HMDistrictModelKey;
extern NSString *const HMDistrictSubtitleKey;

// 排序通知
extern NSString *const HMSortDidChangeNotifacation;
extern NSString *const HMSortModelKey;

// 收藏改变的通知
extern NSString *const HMCollectDidChangeNotification;

// 团购cell的宽度
//extern const CGFloat HMDealCellItemWidth;
