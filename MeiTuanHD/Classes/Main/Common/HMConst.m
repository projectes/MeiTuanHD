//
//  HMConst.m
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/9/6.
//

//#import "HMConst.h"

/**
  常量值
  .m 文件中进行赋值 const: 标识后面定义不可修改的
 */

// 城市通知
NSString *const HMCityDidChangeNotifacation = @"HMCityDidChangeNotifacation";
NSString *const HMCityNameKey = @"HMCityNameKey";

// 分类通知
NSString *const HMCategoryDidChangeNotifacation = @"HMCategoryDidChangeNotifacation";
NSString *const HMCategoryModelKey = @"HMCategoryModelKey";
NSString *const HMCategorySubtitleKey = @"HMCategorySubtitleKey";

// 区域通知
NSString *const HMDistrictDidChangeNotifacation = @"HMDistrictDidChangeNotifacation";
NSString *const HMDistrictModelKey = @"HMDistrictModelKey";
NSString *const HMDistrictSubtitleKey = @"HMDistrictSubtitleKey";

// 排序通知
NSString *const HMSortDidChangeNotifacation = @"HMSortDidChangeNotifacation";
NSString *const HMSortModelKey = @"HMSortModelKey";

// 收藏改变的通知
NSString *const HMCollectDidChangeNotification = @"HMCollectDidChangeNotification";

// 团购cell的宽度
//const CGFloat HMDealCellItemWidth = 305;
