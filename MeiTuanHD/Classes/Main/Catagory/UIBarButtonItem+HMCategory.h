//
//  UIBarButtonItem+HMCategory.h
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/8/30.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIBarButtonItem (HMCategory)

/**
 提供类方法，返回带有高亮图像的 BarButtonItem
 */
+ (instancetype)barbuttonItemWithTarget:(id)target action:(SEL)action icon:(NSString *)icon highlighticon:(NSString *)highlighticon;

@end

NS_ASSUME_NONNULL_END
