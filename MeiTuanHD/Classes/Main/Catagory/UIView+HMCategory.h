//
//  UIView+HMCategory.h
//  05-QQ空间-掌握
//
//  Created by Hanyang Li on 2021/7/5.
//

#import <UIKit/UIKit.h>

@interface UIView (HMCategory)

@property (nonatomic, assign) CGFloat x;
@property (nonatomic, assign) CGFloat y;
@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) CGFloat centerX;
@property (nonatomic, assign) CGFloat centerY;
@property (nonatomic, assign) CGSize size;
@property (nonatomic, assign) CGPoint origin;

@end

