//
//  UIBarButtonItem+HMCategory.m
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/8/30.
//

#import "UIBarButtonItem+HMCategory.h"

@implementation UIBarButtonItem (HMCategory)

/**
 提供类方法，返回带有高亮图像的 BarButtonItem
 */
+ (instancetype)barbuttonItemWithTarget:(id)target action:(SEL)action icon:(NSString *)icon highlighticon:(NSString *)highlighticon{
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:icon] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:highlighticon] forState:UIControlStateHighlighted];
    
   //可以设置成图像的大小
    button.size = button.currentImage.size;
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:button];
}

@end
