//
//  HMDealTool.h
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/9/23.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class  HMDealModel;

@interface HMDealTool : NSObject

/** 添加一条数据  */
+ (void) insertCollectDeal:(HMDealModel *)dealModel;

/** 删除一条数据  */
+ (void) removeCollectDeal:(HMDealModel *)dealModel;

/** 判断数据库是否添加了模型数据 */
+ (BOOL) isCollectDeal:(HMDealModel *)dealModel;

/** 根据传人的页码，返回对应的数据 */
+ (NSArray *) collectDealModelWithPage:(NSInteger)page;

/** 返回数据库的总个数 */
+ (NSInteger)totalCount;

@end

NS_ASSUME_NONNULL_END
