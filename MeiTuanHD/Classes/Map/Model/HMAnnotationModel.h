//
//  HMAnnotationModel.h
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/9/18.
//

#import <Foundation/Foundation.h>
//1.导入框架
#import <MapKit/MapKit.h>

NS_ASSUME_NONNULL_BEGIN

//2.遵守协议
@interface HMAnnotationModel : NSObject<MKAnnotation>

//3.实现属性
// The implementation of this property must be KVO compliant.
@property (nonatomic) CLLocationCoordinate2D coordinate;

@property (nonatomic, copy, nullable) NSString *title;
@property (nonatomic, copy, nullable) NSString *subtitle;

/** icon  属性*/
@property(nonatomic, copy, nullable)NSString *icon;

@end

NS_ASSUME_NONNULL_END
