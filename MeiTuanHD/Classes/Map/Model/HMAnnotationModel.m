//
//  HMAnnotationModel.m
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/9/18.
//

#import "HMAnnotationModel.h"

@implementation HMAnnotationModel

/** 重写此方法，自己指定要比较的部分  */
- (BOOL)isEqual:(HMAnnotationModel *)object
{
    return [self.title isEqualToString:object.title];
}


@end
