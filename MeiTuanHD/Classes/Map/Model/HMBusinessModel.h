//
//  HMBusinessModel.h
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/9/18.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HMBusinessModel : NSObject


/** 经度 */
@property (nonatomic, assign) CGFloat longitude;

/** 纬度 */
@property (nonatomic, assign) CGFloat latitude;

/** 商家名称 */
@property (nonatomic, copy) NSString *name;

/** 商家地址 */
@property (nonatomic, copy) NSString *address;

/** 所属分类信息列表，如[宁波菜，婚宴酒店]*/
@property (nonatomic, strong) NSArray *categories;


@end

NS_ASSUME_NONNULL_END
