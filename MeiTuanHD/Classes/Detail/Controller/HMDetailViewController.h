//
//  HMDetailViewController.h
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/9/10.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class HMDealModel;

@interface HMDetailViewController : UIViewController

/** 模型属性 */
@property (nonatomic, strong) HMDealModel *dealModel;

/** 增加 block 属性，用于收藏按钮点击时的传值 */
@property (nonatomic, copy) void(^detailVCCollectClick)();

@end

NS_ASSUME_NONNULL_END
