//
//  HMRestrictionModel.h
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/9/17.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HMRestrictionModel : NSObject

/** 是否支持随时退款， 0 : 不是 1: 是 */
@property (nonatomic, assign) NSInteger is_refundable;

@end

NS_ASSUME_NONNULL_END
