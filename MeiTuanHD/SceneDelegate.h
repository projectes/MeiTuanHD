//
//  SceneDelegate.h
//  MeiTuanHD
//
//  Created by Hanyang Li on 2021/8/27.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow* window;

@end
